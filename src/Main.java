import java.util.Scanner;
import static java.lang.Math.sqrt;

public class Main {

    public static void main(String[] args) {
        Scanner cititor=new Scanner(System.in);

        System.out.println("Introduceti numarul pentru verificat: ");
        try {
            double toVerify = cititor.nextDouble();
            double aux=sqrt(toVerify);
            if(aux==(int)aux)
                System.out.println("Felicitari, ai nimerit un patrat perfect! (radacina sa este " + (int)aux + ")");
            else
                System.out.println("Numarul " + toVerify + " nu este patrat perfect." );
        }catch(Exception e){
            System.out.println("Probabil nu a fost introdusa o valoare cel mult de tip double :) ");
        }
    }
}
